#!/bin/bash

## From https://stackoverflow.com/questions/25010369/wget-curl-large-file-from-google-drive/39225039#39225039
ggID='SHAREABLE_LINK_ID_HERE' 
ggURL='https://drive.google.com/uc?export=download'  

filename="$(curl -sc /tmp/gcokie "${ggURL}&id=${ggID}" | grep -o '="uc-name.*</span>' | sed 's/.*">//;s/<.a> .*//')"  
getcode="$(awk '/_warning_/ {print $NF}' /tmp/gcokie)"  
curl -LOJb /tmp/gcokie "${ggURL}&confirm=${getcode}&id=${ggID}"