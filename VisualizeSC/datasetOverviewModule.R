library(shiny)

facet2_choices <- c("seurat_clusters")
if("annotated_clusters" %in% GROUP_BY_DEFAULT_CHOICES) {
  facet2_choices <- c("seurat_clusters", "annotated_clusters")
}

datasetOverviewUI <- function(id) {
  ns <- NS(id)
  tagList(
      tabsetPanel(
        tabPanel(title="Overview",
                 h4(textOutput(ns("metadata1")), align="center"),
                 p(),
                 DTOutput(ns("sampleDf"))
        ),
        tabPanel(title="Facet Statistics",
                 #fluidRow(
                   selectInput(ns("FacetA"), label="FacetA", choices=GROUP_BY_DEFAULT_CHOICES, selected=0),
                   selectInput(ns("FacetB"), label="FacetB", choices=GROUP_BY_DEFAULT_CHOICES, selected=1),
                   checkboxInput(ns("normalized"), label = "Normalize_row", value=F)
                 , downloadButton(ns("DownloadButton"), "Download Figure")    
                 #)
                 , p()
                 , fluidRow( plotOutput(ns("facetStatistics")) )
                 )
      )
  )
}

createTableFromDataframe <- function(dataFrameName, columnsToShow, normalize_row=F) {
  inputTable = as.table(table(dataFrameName[, columnsToShow]))
  if(normalize_row) {
    z <- rowSums(inputTable)
    w <- round( inputTable/rowSums( inputTable)*1000)/10.0
    new_rr <- rownames(w)
    for(i in 1:dim(w)[1]) {
      new_rr[i] = paste0(rownames(w)[i], " (", as.character(z[i]), ")")
    }
    rownames(w) <- new_rr
  } else {
    w <- inputTable
  }
  w
}


getFacetHeatmap <- function(w, facets, plotTitle=NULL) {
  wDf <- melt(w)
  colnames(wDf) <- c("Var1", "Var2", "value")

  flog.info("wDf %d x %d columns: %s", dim(wDf)[1], dim(wDf)[2], paste(colnames(wDf), collapse = ", "))
  gg <- ggplot(data=wDf, aes(y=factor(Var1), x=factor(Var2), fill=value))+geom_tile(color="white", size=0.1)
  gg <- gg + coord_equal() + geom_text(aes(label = round(value, 2)))
  gg <- gg + labs(y=facets[1], x=facets[2], title=plotTitle)
  gg <- gg + scale_fill_gradient(low="gray90", high="blue")
  if(require(ggthemes)) { # safe here so as to not break currently built docker_base
    gg <- gg + theme_tufte(base_family="Helvetica")
  }
  gg <- gg + theme(plot.title=element_text(hjust=0))
  gg <- gg + theme(axis.ticks=element_blank())
  gg <- gg + theme(axis.text=element_text(size=10))
  gg <- gg + theme(legend.title=element_text(size=10))
  gg <- gg + theme(legend.text=element_text(size=10))
  gg <- gg + theme(axis.text.x = element_text(angle = 90, hjust = 1))
  gg
}

testGetW <- function() {
  w <- matrix(runif(6)*100.00, nrow = 2, ncol = 3)
  colnames(w) <- c("c1", "c2", "c3")
  rownames(w) <- c("r1", "r2")
  w
}

testGetFacetHeatmap <- function() {
  facets <- c("sample", "cluster")
  w <- testGetW()
  getFacetHeatmap(w, facets)
}


datasetOverview <- function(input, output, session, credentials=NULL, scds=NULL,  sampleDf=NULL) {
  showNotification("loading data", duration = 5)
  output$metadata1 <- renderText({ if(!is.null(scds)) {
    req(credentials()$user_auth)
    str(scds@meta.data)
    # paste("project.name: ",  scds@project.name, "data:", dim(scds)[1], "genes x", dim(scds)[2], "cells", sep=" ")
  } else {
    ""
  }
  })

  facetPlotInput <- reactive({
    facets <- c(input$FacetA, input$FacetB)
    metaDf <- scds@meta.data # merge(scds@meta.data, sampleDf[, c("id", facetsInSampleDf)], by.x="sample", by.y="id")
    table1 <- createTableFromDataframe(metaDf, facets, normalize_row = input$normalized)
    plot1 <- getFacetHeatmap(table1, facets)
    plot1
  })


  output$sampleDf <- renderDT({
    req(credentials()$user_auth)

    if(!is.null(sampleDf)) {
      sampleDf
    }
  })

  observeEvent({
    input$FacetA
    input$FacetB
    input$normalized
  },{
    output$facetStatistics <- renderCachedPlot({  print(facetPlotInput())  },
                                               cacheKeyExpr = { c("facetPlot",
                                                                  input$FacetA,
                                                                  input$FacetB,
                                                                  input$normalized) })
  })
  
  output$DownloadButton <- downloadHandler(
    filename = function() {
      fileName = paste0("facetPlot-", input$FacetA, "-", input$FacetB, "-", "rowNormalized_", input$normalized, '.pdf')
      flog.info("saving to file: %s", fileName)
      fileName
    },
    content = function(file) {
      ggsave(file,facetPlotInput())
    }
  )
}
