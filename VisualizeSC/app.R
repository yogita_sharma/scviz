#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)
## Plot caching to decrease time required for rendering
mcache=memoryCache(max_size=20e6)
# dcache=diskCache(dir="app-cache")
shinyOptions(cache=mcache)

library(Seurat)
library(futile.logger)
library(data.table)
library(DT)
library(stringr)
options(DT.options = list(pageLength = 100))
library(shinyauthr)
library(shinyjs)
library(ggplot2)
library(reshape2)


## This file does all the preprocessing of the data as well as defines default choices  for features, groupBy (allowing better performance)
## For a new dataset, please perform necessary preprocessing in this file.
source("global.R")

### Modules
source("datasetOverviewModule.R")
source("markerOverviewModule.R")
source("seuratDimPlotModule.R")
source("seuratFeaturePlotModule.R")
source("enrichmentModule.R")
source("aboutTab.R")


# Define UI for application that draws a histogram
ui <- fluidPage(
  # must turn shinyjs on
  shinyjs::useShinyjs(),
  # titlePanel(title=div(img(src="SCViz.png", width="32", height="32", alt="SCViz")), windowTitle="SCViz"),
  # add logout button UI
  div(class = "pull-right", shinyauthr::logoutUI(id = "logout")),
  # add login panel UI function
  shinyauthr::loginUI(id = "login"),
  # setup table output to show user info after login
  uiOutput("tabsetPanels")
)


# Define server logic required to draw a histogram
server <- function(input, output, session) {

  # call the logout module with reactive trigger to hide/show
  logout_init <- callModule(shinyauthr::logout,
                            id = "logout",
                            active = reactive(credentials()$user_auth))

  # call login module supplying data frame, user and password cols
  # and reactive trigger
  credentials <- callModule(shinyauthr::login,
                            id = "login",
                            data = user_base,
                            user_col = user,
                            pwd_col = password,
                            log_out = reactive(logout_init()))

  # pulls out the user information returned from login module
  user_data <- reactive({credentials()$info})

  output$tabsetPanels <- renderUI({
    req(credentials()$user_auth)
    tabsetPanel(type="tabs",
              tabPanel("Dataset", datasetOverviewUI("dataset"))
              , tabPanel("Markers", markerOverviewUI("markers"))
              , tabPanel("Clusters", seuratDimPlotUI("dimplot"))
              , tabPanel("Genes", seuratFeaturePlotUI("featureplot"))
              , tabPanel("Enrichment", enrichmentTableUI("enrichmenttable"))
              , tabPanel("About", aboutTab)
    )
  })
  callModule(datasetOverview, "dataset", credentials, scds=cData, sampleDf=sampleDf)
  callModule(markerOverview, "markers", credentials, scds=cData, markers=as.data.frame(cMarkers))
  callModule(seuratDimPlot, "dimplot", credentials, scds=cData, groupByChoices=NULL)
  callModule(seuratFeaturePlot, "featureplot", credentials, scds=cData, groupByChoices=NULL, featureChoices=NULL)
  callModule(enrichmentTable, "enrichmenttable", credentials, markers=as.data.frame(cMarkers))
}

# Run the application
shinyApp(ui = ui, server = server )

