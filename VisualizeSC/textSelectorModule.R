#########################################################################
## textInput Module
#########################################################################
textSelectorInput <- function(
  id, 
  label, 
  placeholder,
  width=6 
) {
  ns <- NS(id)
  tagList(
    column(width=width,
           textInput(
             ns("currInput"),
             label=label,
             placeholder = placeholder
           )
    )
  )
}

textSelector <- function(input, output, session) {
  if(nchar(input$currInput) == 0) { return(NULL) }
  return(input$currInput)
}
