# Provisioning
This directory contains code for provisioning local docker and deploying on GCP. 

### Downloading a prebuilt image
To see the list of prebuilt images available

    docker login --username vjethava82
    docker image ls vjethava82/scviz

### To download and run  an image

   docker login --username vjethava82
   docker pull vjethava82/scviz:nucleiviz

### To run an image

    docker container run --publish 5050:80 vjethava82/scviz:nucleiviz


#### To keep running the image in background mode (detach)

    docker container run --publish 5050:80 --detach vjethava82/scviz:nucleiviz

The running visualizer can be accessed at http://localhost:5050 . 

### Building an image

    ./build_image.sh ../data/nucleiviz [TAG];

If `TAG` is not provided the basename of the directory is taken as the default tag.

### Deploying on GCP 
The following commands allow deployment on gcp
    

    docker tag scviz:latest eu.gcr.io/scviz-258116/nviz:latest; 
    docker push eu.gcr.io/scviz-258116/nviz:latest;
    gcloud config set compute/zone europe-north1-a; 
    gcloud compute instances create-with-container nviz \
        --container-image eu.gcr.io/scviz-258116/nviz:latest  --tags http-server \
        --machine-type=n1-standard-2


    ./build_image.sh ../data/teviz; 
    docker tag scviz:latest eu.gcr.io/scviz-258116/teviz:latest; 
    docker push eu.gcr.io/scviz-258116/teviz:latest;
    gcloud config set compute/zone europe-north1-a; 
    gcloud compute instances create-with-container teviz \
        --container-image eu.gcr.io/scviz-258116/teviz:latest  --tags http-server \
        --machine-type=n1-standard-2
