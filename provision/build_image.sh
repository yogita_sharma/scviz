#!/bin/bash


usage() {
    echo "Usage: ./build_app.sh SCVIZ_DATA_DIR TAG"
    echo "Please provide a valid SCVIZ_DATA_DIR containing the files:"
    echo "   - PanglaoDB_markers_11_Sep_2019.tsv - file containing Panglaodb markers"
    echo "   - processed.rds - file containing either list(SeuratDataset, ListOfMarkers)" 
    echo "     OR dataset.rds = Seurat Dataset AND markers.rds = List of markers"
    echo "   - user_base.rds"
    echo "   - sampleDf.tsv [OPTIONAL]"
    exit 1; 
}
if [ $# -eq 0 ]; then 
    usage; 
elif [ $# -eq 1 ]; then
    TAG=`basename $1`
else
    TAG="$2"
fi
echo "Building app using SCVIZ_DATA_DIR: $1 TAG: $TAG";

####################################################################### 
## update base image
#######################################################################
# docker build ./dockerfile_base -t vjethava82/visualizesc:base
# docker push vjethava82/visualizesc:base

poplocals() {
    rm -rf ./dockerfile_app/app/
    rm -rf ./dockerfile_app/data/
}

poplocals

cp -r ../VisualizeSC ./dockerfile_app/app
cp -r $1 ./dockerfile_app/data 


IMAGE="vjethava82/scviz:$TAG"
docker build ./dockerfile_app -t $IMAGE

poplocals

echo '# Buid complete succesfully.'
echo "# To push to private repository"
echo ""
echo "  docker login --username vjethava82"
echo "  docker push $IMAGE"
echo ""
echo "# To start in detached mode,run the following command:"
echo ""
echo "  docker container run --publish 5050:80 --detach $IMAGE"
echo ""
echo "# To start,run the following command:"
echo ""
echo "  docker container run --publish 5050:80 $IMAGE"
echo ""
