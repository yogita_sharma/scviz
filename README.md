# SCViz
This app makes it easy to single cell datasets using Seurat library by visualizing commonly used plots for genes of interest. The user can choose to download the plots. Currently the following features are supported:

- Cluster: Here user can visualize and download Seurat clustering (umap/tsne) for different groupings

- Genes: Here user can generate and download number of plots for one or more genes split according to the grouping
    - FeaturePlot - for the groupinh, overlay of genes on the umap clustering"
    - DotPlot - for the grouping, see fraction of cells having the gene expressed and the average expression
    - Heatmap - for the grouping, see heatmap over selected genes
    - ViolinPlot - for the grouping, see violin plot of expression levels for the selected genes
    - RidgePlot - new visualization in Seurat 3.0, looks nicer compared to ViolinPlot

## Running
1. For running this code, copy the file `processed.rds`  from GDrive into the `VisualizeSC` sub-directory.
2. One can run the code using the following command

    library("shiny")
    runApp('VisualizeSC')

## Data
When running directly, the 'data' directory should have two files.

1. `processed.rds` - This file is generated from `sc_pipeline` and has the following attributes
2. `sampleDf.tsv` - This file contains all the sample information (OPTIONAL) 
3. `PanglaoDB_markers_11_Sep_2019.tsv` - This file contains PanglaoDB markers 
   
Alternatively, instead of providing `processed.rds`, one can instead provide two files with the following names

- `dataset.rds` - This should be a SeuratObject 
- `markers.rds` - This should be data frame containing list of markers (OPTIONAL)



## Notes 
- Clustering at different resolutions is not supported by design to keep the visualization simple (see `preprocessingScds`)

- Currently, the visualizer loads the `exampleData.rds` file if it is present in the local directory or from Google Drive using the path

    
    EXAMPLE_DATA_FILE=file.path(Sys.getenv("HOME"), "Google Drive", "Yogita", "singleCellRNASequencing", "VisualizeSC", "exampleData.rds")

## Contact
- Yogita Sharma (yogita.sharma@med.lu.se)
- Vinay Jethava (vjethava@gmail.com)
